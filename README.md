# Common BOM

This BOM represents dependencies within the WPI/FRC versioning system.

## Usage

In build.gradle of the project that needs the BOM:

* Update the `repositories` closure to include the requisite Maven repository and the repository of any dependency that you are looking to use:
```gradle
repositories {
    ...
    maven {
        url = "https://gitlab.com/api/v4/groups/chargerrobotics3786/-/packages/maven"
        name = "Charger Robotics Common"
    }
    maven {
        url = "https://frcmaven.wpi.edu/artifactory/release/"
        name = "WPILib Maven"
    }
    maven {
        url = "https://frcmaven.wpi.edu/artifactory/vendor-mvn-release"
        name = "FRC Vendor Maven"
    }
    maven {
        url = "https://broncbotz3481.github.io/YAGSL-Lib/yagsl/repos"
        name = "YAGSL Maven"
    }
    maven {
        url = "https://maven.reduxrobotics.com/"
        name = "Redux Robotics Maven"
    }
    maven {
        url = "https://maven.ctr-electronics.com/release/"
        name = "Phoenix 6 Maven"
    }
    maven {
        url = "https://dev.studica.com/maven/release/2025/"
        name = "Studica Maven"
    }
    maven {
        url = "https://shenzhen-robotics-alliance.github.io/maple-sim/vendordep/repos/releases"
        name = "Shenzhen Robotics Maven"
    }
    maven {
        url = "https://docs.home.thethriftybot.com"
        name = "Thrifty Bot Maven"
    }
    ...
}
```

* Add a platform implementation for the BOM along with the implementation of any dependency you want to use to the `dependencies` closure
```gradle
dependencies {
    ...
    implementation platform('com.chargerrobotics.common:common-bom:<BOM_VERSION>')

    implementation "edu.wpi.first.wpilibNewCommands:wpilibNewCommands-java"
    ...
}
```
